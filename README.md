
# Rs Prod Project Template
**Шаблон проекта для быстрого старта**

## Старт проекта

### Склонируйте репозиторий и перейдите в папку проекта
```
git clone https://RsProd@bitbucket.org/RsProd/technosoft-project.git ts-project && cd ts-project
```

### Установите модули

```
npm i
```

### Запустите шаблон
```
gulp
```

## Структура папок и файлов
```
├── dev/                            			# Исходники
│   ├── css/                        			# Стили CSS
│   │   └── main.css                			# Основной файл css стилей
│   ├── icon/                       			# SVG иконки для генерации спрайта
│   ├── images/                     			# Изображения для оптимизации 
│   ├── js/                         			# Скрипты
│   │   └── main.js                 			# Главный скрипт
│   ├── scss/                       			# Стили
│   │   ├── components/             			# Компоненты
│   │   │   └── basic/              			# Базовые компоненты
│   │   │       ├── base.scss       			# Базовые стили
│   │   │       ├── grid.scss       			# Сетка flexbox grid
│   │   │       ├── reset.scss      			# Сброс стилей
│   │   │       └── utils.scss      			# Переменные и миксины
│   │   └── main.scss               			# Главный стилевой файл
│   ├── template/                   			# Шаблоны pug
│   │   └── includes/               			# Папка включаемых компонентов pug
│   │       ├── footer.pug          			# Содержание подвала
│   │       ├── head.pug            			# Содержание тега head
│   │       ├── header.pug          			# Содержание шапки
│   │       └── mixin.pug           			# Миксины
├── public/                         			# Корневая папка проекта
│   ├── css/                        			# Минифицирование файлы стилей
│   ├── icon/                       			# SVG спрайт
│   ├── images/                     			# Изображения
│   ├── js/                         			# Скрипты
│   └── index.html                  			# Страница
├── tasks/                          			# Подключаемые скрипты с задачами для gulpfile.js
│   ├── default.js                  			# Итоговые списки задач по умолчанию
│   ├── deploy.js                   			# Деплой на удаленный сервер
│   ├── icons.js                    			# Сборка SVG иконок в один файл
│   ├── images.js                   			# Оптимизация изображений
│   ├── script.js                   			# Сборка скриптов
│   ├── server.js                   			# Запуск локального сервера
│   ├── styles.js                   			# Сборка стилей
│   └── templates.js                			# Сборка страниц из Pug шаблонов
├── .gitignore                      			# Список исключённых файлов из Git
├── settings.json                          		# Файл настроек проекта
├── gulpfile.js                     			# Файл для запуска Gulp
├── package.json                    			# Список модулей и прочей информации
└── readme.md                       			# Документация шаблона
```

### Команды для запуска FTP соединения
### Разовая выгрузка
```
gulp deploy
```
Разовая выгрузка на сервер, после запуска команды, в указной директории, будет сознада папка "public"
(Например: /site.ru/public_html/wp-content/themes/ имя темы wordpress будет public)

### Отслеживание и отправка измененных файлов
```
gulp deploy-watch
```

*Для индексации и загрузки новых файлов в директорию "public", необходимо запустить команду "gulp deploy"*

При роботе с ftp, рекомендуется использовать одновременно два окна терминала, в первом запускать "gulp", а во втором "gulp deploy-watch".

## Список используемых плагинов gulp
```
autoprefixer									# Плагин PostCSS для анализа CSS и добавления префиксов.
browser-sync									# Live CSS Reload и Browser Syncing
fs												# Виниловый адаптер для файловой системы.			
gulp-cheerio 									# Манипулятор SVG, HTML и XML-файлами
gulp-concat										# Объединение файлов
gulp-cssnano       								# Минификация CSS
gulp-imagemin      								# Оптимизатор изображений
gulp-plumber									# Предотвращение вылетов gulp из-за ошибок javascript
gulp-postcss									# Построцессор CSS
gulp-pug										# Шаблонизатор HTML
gulp-rename										# Переименование
gulp-sass										# Компилятор SASS
gulp-svgmin										# Минификация SVG
gulp-svgstore									# Объединение svg-файлов в спрайт
gulp-uglify										# Минификация javascript
gulp-util										# Полезные функции для плагинов gulp
path											# node.js модуль пути
require-dir										# Плагин для подключения tasks отдельными файлами
vinyl-ftp										# Виниловый адаптер для FTP
```
