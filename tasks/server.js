var gulp = require('gulp'),
    fs = require('fs'),
    settings = JSON.parse(fs.readFileSync('settings.json')),
    browserSync = require('browser-sync').create();

gulp.task('server', function() {
    browserSync.init({
        server: settings.local_path + "/",
        port: settings.bs_port,
        proxy: settings.bs_proxy,
        open: settings.bs_open,
        localOnly: settings.bs_localOnly,
        codeSync: settings.bs_codeSync,
        notify: settings.bs_notify,
        reloadDelay: settings.bs_reloadDelay,
        reloadThrottle: settings.bs_reloadThrottle,
        files: [
            settings.local_path + "/*.html",
            settings.local_path + "/*.php",
            settings.local_path + "/css/*.css",
            settings.local_path + "/js/*.js",
            settings.local_path + "/icon/*.svg"
        ]
    });
});