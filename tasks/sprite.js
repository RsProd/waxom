var gulp = require('gulp'),
    fs = require('fs'),
    settings = JSON.parse(fs.readFileSync('settings.json')),
    spritesmith = require('gulp.spritesmith');

gulp.task('sprite', function() {
    var spriteData = 
        gulp.src('dev/images/*.*') // путь, откуда берем картинки для спрайта
            .pipe(spritesmith({
                imgName: 'sprite.png',
                cssName: '_sprite.scss',
                cssFormat: 'scss',
                algorithm: 'binary-tree',
                imgPath: '../images/sprite.png',
                cssVarMap: function(sprite) {
                    sprite.name = 's-' + sprite.name
                }
            }));

    spriteData.img.pipe(gulp.dest(settings.local_path + '/images')); // путь, куда сохраняем картинку
    spriteData.css.pipe(gulp.dest('dev/scss/components/sprite/')); // путь, куда сохраняем стили
});