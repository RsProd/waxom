   var gulp = require('gulp'),
       fs = require('fs'),
       settings = JSON.parse(fs.readFileSync('settings.json')),
       svg = require('gulp-svgstore'),
       svgmin = require('gulp-svgmin'),
       cheerio = require('gulp-cheerio'),
       rename = require('gulp-rename'),
       path = require('path');

   gulp.task('svg', function() {
       return gulp
           .src('dev/icon/*.svg')
           .pipe(svgmin(function(file) {
               var prefix = path.basename(file.relative, path.extname(file.relative));
               return {
                   plugins: [{
                       cleanupIDs: {
                           prefix: prefix + '-',
                           minify: true
                       }
                   }]
               }
           }))
           .pipe(cheerio({
               parserOptions: { xmlMode: true }
           }))
           .pipe(rename({ prefix: 'icon_' }))
           .pipe(svg())
           .pipe(rename('sprite.svg'))
           .pipe(gulp.dest(settings.local_path + '/icon'));
   });