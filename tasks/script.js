var gulp = require('gulp'),
    fs = require('fs'),
    settings = JSON.parse(fs.readFileSync('settings.json')),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    plumber = require('gulp-plumber');

// js
gulp.task('js', function() {
    return gulp.src('dev/js/*.js')
        .pipe(plumber())
        .pipe(concat('main.js'))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(uglify())
        .pipe(gulp.dest(settings.local_path + '/js'));
});
