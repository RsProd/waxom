 var gulp = require('gulp'),
     fs = require('fs'),
     settings = JSON.parse(fs.readFileSync('settings.json')),
     plumber = require('gulp-plumber'),
     pug = require('gulp-pug');

 gulp.task('pug', function() {
     gulp.src('dev/template/*.pug')
         .pipe(plumber())
         .pipe(pug({
             pretty: true,
         }))
         .pipe(gulp.dest(settings.local_path + '/'));
 });