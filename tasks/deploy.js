var gulp = require('gulp'),
    fs = require('fs'),
    settings = JSON.parse(fs.readFileSync('settings.json')),
    gutil = require('gulp-util'),
    ftp = require('vinyl-ftp');

var globalFolder = './' + settings.local_path + '/**/*';


function getFtpConnection() {
    return ftp.create({
        host: settings.host,
        user: settings.user,
        password: settings.pass,
        parallel: 10,
        log: gutil.log
    });
}


function getFtpConnectionSecond() {
    return ftp.create({
        host: settings.host_second,
        user: settings.user_second,
        password: settings.pass_second,
        parallel: 10,
        log: gutil.log
    });
}


gulp.task('deploy', function() {
    var conn = getFtpConnection();
    return gulp.src(globalFolder, { buffer: false })
        .pipe(conn.newer(settings.path)) 
        .pipe(conn.dest(settings.path));
});


gulp.task('deploy-second', function() {
    var conn = getFtpConnectionSecond();
    return gulp.src(globalFolder, { buffer: false })
        .pipe(conn.newer(settings.path_second))
        .pipe(conn.dest(settings.path_second));
});



gulp.task('deploy-watch', function() {
    var conn = getFtpConnection();
    gulp.watch(globalFolder)
        .on('change', function(event) {
            return gulp.src([event.path], { base: settings.local_path, buffer: false })
                .pipe(conn.newer(settings.path))
                .pipe(conn.dest(settings.path));
        });
});

gulp.task('deploy-watch-second', function() {
    var conn = getFtpConnectionSecond();
    gulp.watch(globalFolder)
        .on('change', function(event) {
            return gulp.src([event.path], { base: settings.local_path, buffer: false })
                .pipe(conn.newer(settings.path_second))
                .pipe(conn.dest(settings.path_second));
        });
});


