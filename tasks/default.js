var gulp = require('gulp');

gulp.task('default', ['server', 'pug', 'sass', 'css', 'js', 'svg', 'sprite'], function() {
	gulp.watch('dev/images/*', ['sprite']);
    gulp.watch('dev/template/**/*.pug', ['pug']);
    gulp.watch('dev/scss/**/*.scss', ['sass']);
    gulp.watch('dev/css/*.css', ['css']);
    gulp.watch('dev/js/*.js', ['js']);
    gulp.watch('dev/icon/*.svg', ['svg']);
});