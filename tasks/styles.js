var gulp = require('gulp'),
    fs = require('fs'),
    settings = JSON.parse(fs.readFileSync('settings.json')),
    sass = require('gulp-sass'),
    postcss = require('gulp-postcss'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    cssnano = require('gulp-cssnano'),
    autoprefixer = require('autoprefixer');

// sass
gulp.task('sass', function() {
    return gulp.src('dev/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('dev/css'));
});

// css
gulp.task('css', function() {
    return gulp.src('dev/css/*.css')
        .pipe(concat('main.css'))
        .pipe(postcss([autoprefixer({
            browsers: ['last 15 versions']
        })]))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(cssnano())
        .pipe(gulp.dest(settings.local_path + '/css'));
});
