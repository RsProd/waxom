$(window).load(function() {
    $('.preloader').addClass('preloader_load');
    setTimeout(function() {
        $('.preloader').remove();
    }, 800);
});


$(document).ready(function() {
    o = $('.rd-navbar');
    o.RDNavbar({
        stickUpClone: false,
        stickUp: false,
        domAppend: true,
        responsive: {
            0: {
                layout: 'rd-navbar-fixed',
                stickUp: false
            },
            768: {
                layout: 'rd-navbar-fixed',
                stickUp: false
            },
            993: {
                layout: 'rd-navbar-static',
                stickUp: false
            },
            1200: {
                layout: 'rd-navbar-static',
                stickUp: false
            }
        }
    });

    $(function() {
        var headSlider = $('.slider');
        headSlider.owlCarousel({
            loop: true,
            margin: 0,
            nav: true,
            dots: true,
            items: 1,
            autoplay: false,
            navText: [],
            autoplayTimeout: 3000,
            smartSpeed: 500
        });
    });

    $('.grid').imagesLoaded(function() {
        var $grid = $('.grid').masonry({});
        var filterFns = {
            numberGreaterThan50: function() {
                var number = $(this).find('.number').text();
                return parseInt(number, 10) > 50;
            },
            ium: function() {
                var name = $(this).find('.name').text();
                return name.match(/ium$/);
            }
        }

        function getHashFilter() {
            var matches = location.hash.match(/filter=([^&]+)/i);
            var hashFilter = matches && matches[1];
            return hashFilter && decodeURIComponent(hashFilter);
        }

        var $filterButtonGroup = $('.button-group');
        $filterButtonGroup.on('click', 'button', function() {
            var filterAttr = $(this).attr('data-filter');
            location.hash = 'filter=' + encodeURIComponent(filterAttr);
        });


        function onHashchange() {
            isIsotopeInit = true;
            var hashFilter = getHashFilter();
            if (!hashFilter && isIsotopeInit) {
                return;
            }

            $grid.isotope({
                itemSelector: '.grid__item',
                filter: filterFns[hashFilter] || hashFilter
            });

            if (hashFilter) {
                $filterButtonGroup.find('.is-checked').removeClass('is-checked');
                $filterButtonGroup.find('[data-filter="' + hashFilter + '"]').addClass('is-checked');
            }
        }
        $(window).on('hashchange', onHashchange);
        onHashchange();
    });

    $('.menu > li > a').click(function(e) {

        if ($("html").hasClass("rd-navbar-static-linked")) {
            var href = $(this).attr("href"),
                offsetTop = href === "#" ? 0 : $(href).offset().top;
        } else {
            var href = $(this).attr("href"),
                offsetTop = href === "#" ? 0 : $(href).offset().top - 56;
        }
        $('html, body').stop().animate({
            scrollTop: offsetTop
        }, 500);
        e.preventDefault();
    });

});

$(window).on('scroll', function() {
    var blockScrolled = $('.scrolled');
    if ($(window).scrollTop() > blockScrolled.offset().top - 600) {
        $(".counters > div").each(function() {
            var src = $(this).find('.counter__num'),
                from = src.text(),
                speed = src.attr('data-speed'),
                countingTo = src.attr('data-counting-to'),
                interval = src.attr('data-interval');
            src.countTo({
                from: Number(from),
                to: Number(countingTo),
                speed: Number(speed),
                refreshInterval: Number(interval),
                onComplete: function(value) {
                    console.debug(this);
                }
            });
        });
        $(window).off('scroll');
    }
});